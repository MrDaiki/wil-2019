package eu.telecomnancy.Person;

import java.util.Calendar;

import javax.validation.Constraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import eu.telecomnancy.Contraint.ageConstraint;


public class Person {

    @NotNull
    @Size(min = 1)
    private String name;

    @ageConstraint
    private Calendar birthDate;

    @NotNull
    @Size(min=2,max=2)
    private String nationality;

    public Person(String name,Calendar c,String nat){
        
        this.name = name;
        this.birthDate = c;
        this.nationality = nat;

    }

}