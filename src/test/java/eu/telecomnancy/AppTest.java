package eu.telecomnancy;

import org.junit.Test;

import eu.telecomnancy.Person.Person;

import static org.junit.Assert.*;

import java.time.Month;
import java.util.Calendar;
import java.util.Set;

import javax.validation.ConstraintValidatorFactory;
import javax.validation.ConstraintViolation;
import javax.validation.MessageInterpolator;
import javax.validation.TraversableResolver;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorContext;
import javax.validation.ValidatorFactory;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test.
     */
    @Test
    public void testApp() {
        
        Calendar c =Calendar.getInstance();
        c.set(1964, 4, 13);

        Person p_ok = new Person("Quentin",c,"FR");
        
        ValidatorFactory v = Validation.buildDefaultValidatorFactory();
        Validator valid = v.getValidator();

        Set<ConstraintViolation<Person>> constraints_ok = valid.validate(p_ok); 

        assertEquals(0, constraints_ok.size());

    }


}
